package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
      //  Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Login");
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(40);
        grid.setPadding(new Insets (25,25,25,25));
        Scene scene = new Scene(grid, 300,275);
        Text sceneTitle = new Text("Please login");
        Label userName = new Label("Username");
        TextField userTextField = new TextField();

        Label pw = new Label("Password");
        PasswordField pwBox = new PasswordField();

        grid.add(sceneTitle,1,0,2,1);
        grid.add(userName,0,1);
        grid.add(userTextField,1,1);
        grid.add(pw,0,2);
        grid.add(pwBox,1,2);

       // grid.setGridLinesVisible(true);

        scene.getStylesheets().add("sample/Login.css");

        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
